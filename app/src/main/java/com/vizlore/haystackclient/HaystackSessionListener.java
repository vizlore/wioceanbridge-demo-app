/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.util.Log;

import org.alljoyn.bus.SessionListener;

/**
 * Created by Pedja on 2/25/2016.
 */
public class HaystackSessionListener extends SessionListener {

    private HaystackClientApplication m_app;

    public HaystackSessionListener(HaystackClientApplication app) {
        super();
        m_app = app;
    }

    @Override
    public void sessionLost(int sessionId, int reason) {
        super.sessionLost(sessionId, reason);
        Log.d("haystackClient", "Session Lost!");



        m_app.setSessionLost(true);
    }
}
