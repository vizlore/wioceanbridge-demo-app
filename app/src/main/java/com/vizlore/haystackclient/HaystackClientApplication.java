/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.app.Application;
import android.os.HandlerThread;
import android.util.Log;
import android.widget.ArrayAdapter;

import org.alljoyn.about.client.AboutClient;
import org.alljoyn.bus.AboutObjectDescription;
import org.alljoyn.bus.AboutProxy;
import org.alljoyn.bus.BusAttachment;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.ErrorReplyBusException;
import org.alljoyn.bus.ProxyBusObject;
import org.alljoyn.bus.SessionOpts;
import org.alljoyn.bus.Status;
import org.alljoyn.bus.alljoyn.DaemonInit;
import org.alljoyn.services.common.AnnouncementHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;


/**
 * Created by Pedja on 2/23/2016.
 */
public class HaystackClientApplication extends Application {
    private BusAttachment m_Bus;
    private AboutProxy m_aboutProxy;
    private HaystackClientAboutListener m_aboutListener;
    private HashMap<String, SoftAPDetails> m_devicesMap;
    private ArrayList<SoftAPDetails> m_services;
    private HaystackClientActivity m_activity;
    private SessionActivity m_sessionActivity;
    private HaystackSessionListener m_haystackSessionListener;
    private HaystackOnSessionJoinListener m_haystackOnSessionJoinListener;
    private ProxyBusObject m_proxyObj;
    private SoftAPDetails m_targetService;
    private HaystackService m_haystackInterface;
    private int m_sessionId;
    private static final String TAG = "haystackClient";
    private boolean sessionLost;

    static {
        try {
            System.loadLibrary("alljoyn_java");
        }
        catch (Exception e) {
            Log.e(TAG, "Cannot load alljoyn_java lib!");
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();

        HandlerThread busThread = new HandlerThread("BusHandler");
        busThread.start();

        sessionLost = false;

        m_devicesMap = new HashMap<String, SoftAPDetails>();
        m_haystackSessionListener = new HaystackSessionListener(this);
        m_haystackOnSessionJoinListener = new HaystackOnSessionJoinListener();
    }

    public void doConnect() {
        boolean b = DaemonInit.PrepareDaemon(getApplicationContext());
        System.out.println(b);
        String ss = getPackageName();
        m_Bus = new BusAttachment(ss, BusAttachment.RemoteMessage.Receive);

        Status status = m_Bus.connect();
        Log.d(TAG, "bus.connect status: " + status);

        m_Bus.setDaemonDebug("ALL", 7);
        m_Bus.setLogLevels("ALL=7");
        m_Bus.useOSLogging(true);

        m_aboutListener = new HaystackClientAboutListener(this, m_devicesMap);
        m_Bus.registerAboutListener(m_aboutListener);

        String[] interfaces = new String[1];
        interfaces[0] = "com.vizlore.haystack.service.interface";
        m_Bus.whoImplements(interfaces);
    }

    public void doDisconnect() {
        Log.d(TAG, "application closing!");
        if (m_aboutListener != null) {
            m_Bus.unregisterAboutListener(m_aboutListener);
        }
        m_Bus.disconnect();
    }

    public void setMainActivityRef(HaystackClientActivity activity) {
        m_activity = activity;
    }

    public void setSessionActivityRef(SessionActivity activity) {
        m_sessionActivity = activity;
    }


    public void setServicesListRef(ArrayList<SoftAPDetails> services) {
        m_services = services;
    }

    public void addDevice(String deviceId, String busName, short port, String deviceFriendlyName, AboutObjectDescription[] interfaces, AboutData aboutData) {
        SoftAPDetails oldDevice = m_devicesMap.get(deviceId);

        if (oldDevice != null) {
            oldDevice.busName = busName;
            oldDevice.aboutData = aboutData;
            if (oldDevice.deviceFriendlyName != deviceFriendlyName) {

                oldDevice.deviceFriendlyName = deviceFriendlyName;
            }
            oldDevice.port = port;
            oldDevice.interfaces = interfaces;
            oldDevice.updateSupportedServices();
        }
        else {
                SoftAPDetails sad = new SoftAPDetails(deviceId, busName, deviceFriendlyName, port, interfaces, aboutData);
                m_devicesMap.put(deviceId, sad);
                Log.d(TAG, deviceFriendlyName);
                m_services.add(sad);
                m_activity.updateServicesList();
        }
    }

    public SoftAPDetails getDeviceByPos(int position) {
        return m_services.get(position);
    }

    public void joinSession(SoftAPDetails service) {
        SessionOpts sessionOpts = new SessionOpts(SessionOpts.TRAFFIC_MESSAGES, false, SessionOpts.PROXIMITY_ANY, SessionOpts.TRANSPORT_ANY);
        try {
            Status status = m_Bus.joinSession(service.busName, service.port, sessionOpts, m_haystackSessionListener, m_haystackOnSessionJoinListener, m_sessionActivity);
            if (status == Status.OK) {
                setSessionLost(false);
            }
            else {
                setSessionLost(true);
            }
            m_targetService = service;
            Log.d(TAG, "Called JoinSession");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pingServices() {
        for (Iterator<SoftAPDetails> iterator = m_services.iterator(); iterator.hasNext();) {
            SoftAPDetails sad = iterator.next();

            Status status = m_Bus.ping(sad.busName, 3000);
            if (status != Status.OK) {
                iterator.remove();
                Log.d("haystackClient", "Lost service: " + sad.busName);
            }
        }
        m_activity.updateServicesList();
    }

    public void setSessionId(int sessionId) {
        m_sessionId = sessionId;
    }

    public void leaveSession() {
        Status status = m_Bus.leaveJoinedSession(m_sessionId);
        if (status != Status.OK) {
            Log.e(TAG, "Error while leaving session: " + status.toString());
        }
        Log.d(TAG, "Left session!");
    }

    public void createProxyObject() {
        m_proxyObj = m_Bus.getProxyBusObject(m_targetService.busName, "/haystack/service", m_sessionId, new Class[] { HaystackService.class});
        m_haystackInterface = m_proxyObj.getInterface(HaystackService.class);
    }

    public String read(String filter) {
        try {
            return m_haystackInterface.read(filter);
        }
        catch (BusException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String watchSub(String watchDis, String id) {
        try {
            return m_haystackInterface.watchSub(watchDis, id);
        }
        catch (BusException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String watchPoll(String watchID ) {
        try {
            return m_haystackInterface.watchPoll(watchID);
        }
        catch (BusException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String pointWrite(String dis, String value) {
        try {
            return m_haystackInterface.pointWrite(dis, 16, value, "android", "M");
        }
        catch (BusException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void setSessionLost(boolean value) {
        sessionLost = value;
    }

    public boolean getSessionLost() {
        return sessionLost;
    }
}
