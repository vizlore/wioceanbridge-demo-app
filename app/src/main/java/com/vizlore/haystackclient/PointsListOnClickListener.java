/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

/**
 * Created by Pedja on 2/25/2016.
 */
public class PointsListOnClickListener implements AdapterView.OnItemClickListener {

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (((SessionActivity)view.getContext()).isSessionLost()) {
            Intent intent = new Intent(view.getContext(), HaystackClientActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            ((SessionActivity)view.getContext()).finish();
            ((SessionActivity)view.getContext()).startActivity(intent);
            return;
        }
        String dis = (String)parent.getAdapter().getItem(position);
        Log.d("haystackClient", "Kliknuo na " + dis);
        Intent intent = new Intent(view.getContext(), PointActivity.class);
        intent.putExtra("dis", dis);
        view.getContext().startActivity(intent);
    }
}
