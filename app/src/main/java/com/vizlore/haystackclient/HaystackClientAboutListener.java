/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.content.Intent;
import android.util.Log;

import org.alljoyn.bus.AboutListener;
import org.alljoyn.bus.AboutObjectDescription;
import org.alljoyn.bus.BusException;
import org.alljoyn.bus.Variant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Pedja on 2/23/2016.
 */
public class HaystackClientAboutListener implements AboutListener {

    // Reference to the devices map, which needs to be updated every time we catch an announce signal.
    private HashMap<String, SoftAPDetails> m_devicesMap;
    private HaystackClientApplication m_app;

    private static final String TAG = "haystackClient";

    public HaystackClientAboutListener(HaystackClientApplication app, HashMap<String, SoftAPDetails> devicesMap) {
        super();
        m_devicesMap = devicesMap;
        m_app = app;
    }

    @Override
    public void announced(String busName, int version, short port, AboutObjectDescription[] interfaces, Map<String, Variant> aboutData) {

            AboutData ab = new AboutData();

            try {
                ab.createFromAnnouncedAboutData(aboutData);
                String deviceId = ab.getDeviceId();
                String deviceFriendlyName = ab.getDeviceName();
                Log.d(TAG, "onAnnouncement received: with parameters: busName:" + busName + ", port:" + port + ", deviceid" + deviceId + ", deviceName:" + deviceFriendlyName);
                m_app.addDevice(deviceId, busName, port, deviceFriendlyName, interfaces, ab);
            }
            catch (BusException e) {
                e.printStackTrace();
            }
    }
}
