package com.vizlore.haystackclient;
/******************************************************************************
 * Copyright AllSeen Alliance. All rights reserved.
 *
 *    Permission to use, copy, modify, and/or distribute this software for any
 *    purpose with or without fee is hereby granted, provided that the above
 *    copyright notice and this permission notice appear in all copies.
 *
 *    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 *    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 *    ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 *    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 *    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 *    OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 ******************************************************************************/

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.alljoyn.bus.AboutKeys;
import org.alljoyn.bus.AboutObjectDescription;
import org.alljoyn.bus.BusException;
import org.alljoyn.services.common.BusObjectDescription;

/**
 * This class represents an AllJoyn device.
 */
public class SoftAPDetails{

    /**
     * The device id.
     */
    String appId;//The device UUID

    /**
     * The device bus name
     */
    String busName;

    /**
     * The device friendly name
     */
    String deviceFriendlyName;

    /**
     * The device port
     */
    short port;

    /**
     * The device supported interfaces
     */
    AboutObjectDescription[] interfaces;

    /**
     * Indicates whether the device support the about service
     */
    boolean supportAbout = false;

    /**
     * Indicates whether the device support the icon service
     */
    boolean supportIcon = false;

    /**
     * Represents the the device about fields as we got in the announcement.
     */
    AboutData aboutData;

    /**
     * Represents the device about languages - the language in which the device requests the about data.
     */
    String aboutLanguage = "";

    String appName = "";

    //===================================================================

    /**
     * Create a device according to the following parameters.
     * @param appId The device id
     * @param busName The device bus name
     * @param deviceFriendlyName The device friendly name
     * @param port The device port
     * @param interfaces The device supported interfaces
     * @param aboutData The device about data
     */
    public SoftAPDetails(String appId, String busName, String deviceFriendlyName, short port, AboutObjectDescription[] interfaces, AboutData aboutData) {

        this.appId = appId;
        this.busName = busName;
        this.deviceFriendlyName = deviceFriendlyName;
        this.port = port;
        this.interfaces = interfaces;
        this.aboutData = aboutData;
        if(aboutData != null){
            try {
                this.aboutLanguage = aboutData.getDefaultLanguage();
                this.appName = aboutData.getAppName();
            }
            catch (BusException e) {
                e.printStackTrace();
            }
        }
        updateSupportedServices();
    }
    //===================================================================

    /**
     * Initialize the device supported services according to the device bus object description.
     */
    public void updateSupportedServices(){

        if(interfaces != null){
            for(int i = 0; i < interfaces.length; ++i){
                AboutObjectDescription temp = interfaces[i];
                String[] supportedInterfaces = temp.interfaces;
                for(int j = 0; j < supportedInterfaces.length; ++j){
                    String interface1 = supportedInterfaces[j];
                    if(interface1.equals("org.alljoyn.About"))
                        this.supportAbout = true;
                    else if(interface1.startsWith("org.alljoyn.Icon"))
                        this.supportIcon = true;

                }
            }
        }
    }
    //===================================================================

    /**
     * @return Returns the device announcement
     */
    public String getAnnounce() {
        StringBuilder sb = new StringBuilder();
        //device name
        sb.append("BusName: "+busName+"\n\n");
        //port
        sb.append("Port: "+port+"\n\n");
        //about map
        if(aboutData == null){
            sb.append("About data:\n");
            sb.append("About data is null\n");
            sb.append("\n");
        }
        else{
            //sb.append("About map:\n");
            //sb.append(aboutData.toString());
            //sb.append("\n");
        }
        //interfaces
        sb.append("Bus Object Description:\n");
        for(int i = 0; i < interfaces.length; i++){
            sb.append(aboutObjectDescriptionString(interfaces[i]));
            if(i != interfaces.length-1)
                sb.append("\n");
        }
        return sb.toString();
    }
    //===================================================================
    public String aboutObjectDescriptionString(AboutObjectDescription bus) {

        String s = "";
        s += "Path: "+bus.path+"\n";
        s += "Interfaces: ";
        String[] tmp = bus.interfaces;
        for (int i = 0; i < tmp.length; i++){
            s += tmp[i];
            if(i != tmp.length-1)
                s += ",";
            else
                s += "\n";
        }
        return s;
    }
    //===================================================================

    public AboutData getAboutData() {
        return aboutData;
    }
}