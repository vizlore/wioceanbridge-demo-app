/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.alljoyn.bus.BusAttachment;

import java.util.ArrayList;


public class HaystackClientActivity extends AppCompatActivity {

    private static BusAttachment m_bus = null;
    private static HaystackClientApplication m_app = null;
    private static ArrayList<SoftAPDetails> m_services = new ArrayList<SoftAPDetails>();
    private static ArrayList<SoftAPDetails> m_servicesAdapter = new ArrayList<SoftAPDetails>();
    private Runnable run;

    private TextView tvServicesList;

    private static final String TAG = "haystackClient";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_haystack_client);
        m_app = (HaystackClientApplication)getApplication();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        tvServicesList = (TextView) findViewById(R.id.tvServicesList);
        tvServicesList.setVisibility(View.VISIBLE);

        final ServicesAdapter servicesAdapter = new ServicesAdapter(this, m_servicesAdapter);

        ListView listView = (ListView) findViewById(R.id.lvAllJoynServices);
        listView.setAdapter(servicesAdapter);
        listView.setOnItemClickListener(new ServicesListOnClickListener());

        m_app.setServicesListRef(m_services);
        m_app.setMainActivityRef(this);

        run = new Runnable() {
            @Override
            public void run() {
                m_servicesAdapter.clear();
                m_servicesAdapter.addAll(m_services);
                servicesAdapter.notifyDataSetChanged();
                if (m_services.size() == 0) {
                    TextView services = (TextView) findViewById(R.id.tvServicesList);
                    services.setText("No services found!");
                }
                else {
                    TextView services = (TextView) findViewById(R.id.tvServicesList);
                    services.setText("List of found AllJoyn services:");
                }
            }
        };

        if (m_app.getSessionLost()) {
            Toast.makeText(m_app.getBaseContext(), "Session Lost!", Toast.LENGTH_LONG).show();
            m_app.setSessionLost(false);
        }

        AllJoynConnect();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_haystack_client, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        m_app.pingServices();
        m_app.leaveSession();
    }

    @Override
    protected void onStart() {
        super.onStart();
        m_app.pingServices();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        m_servicesAdapter.clear();
        m_app.doDisconnect();
    }

    public void AllJoynConnect() {
        m_app.doConnect();
    }

    public void updateServicesList() {
        runOnUiThread(run);
    }
}
