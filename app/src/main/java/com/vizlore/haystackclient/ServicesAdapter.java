/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.alljoyn.bus.BusException;

import java.util.ArrayList;

/**
 * Created by Pedja on 2/24/2016.
 */
public class ServicesAdapter extends ArrayAdapter<SoftAPDetails> {
    public ServicesAdapter(Context context, ArrayList<SoftAPDetails> services) {
        super(context, 0, services);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SoftAPDetails service = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.services_list_item, parent, false);
        }

        TextView tvAppName = (TextView) convertView.findViewById(R.id.tvAppName);
        TextView tvDeviceName = (TextView) convertView.findViewById(R.id.tvDeviceName);

        try {
            tvAppName.setText(service.getAboutData().getAppName());
            tvDeviceName.setText(service.getAboutData().getDeviceName());
        }
        catch (BusException e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
