/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.util.Log;

import org.alljoyn.bus.OnJoinSessionListener;
import org.alljoyn.bus.SessionOpts;
import org.alljoyn.bus.Status;

/**
 * Created by Pedja on 2/25/2016.
 */
public class HaystackOnSessionJoinListener extends OnJoinSessionListener {

    public HaystackOnSessionJoinListener() {
        super();
    }

    @Override
    public void onJoinSession(Status status, int sessionId, SessionOpts opts, Object context) {
        super.onJoinSession(status, sessionId, opts, context);
        if (status != Status.OK) {
            Log.d("haystackClient", "Join session failed!");
            SessionActivity activity = (SessionActivity)context;
            HaystackClientApplication app = (HaystackClientApplication)activity.getApplication();

            activity.m_handler.obtainMessage(0).sendToTarget();
            return;
        }
        Log.d("haystackClient", "Joined session " + sessionId + "!");

        SessionActivity activity = (SessionActivity)context;
        HaystackClientApplication app = (HaystackClientApplication)activity.getApplication();

        activity.m_handler.obtainMessage(1).sendToTarget();

        app.setSessionId(sessionId);
        app.createProxyObject();
    }
}
