/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.projecthaystack.HDict;
import org.projecthaystack.HGrid;
import org.projecthaystack.HRow;
import org.projecthaystack.HVal;
import org.projecthaystack.io.HZincReader;

import java.util.Timer;
import java.util.TimerTask;

public class PointActivity extends AppCompatActivity {
    private String m_pointDis;
    private HaystackClientApplication m_app;
    private String m_watchId;
    private static Timer m_timer;
    private TextView m_tvCurrentValue;
    private String m_currentValue;
    public Handler m_handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        m_app = (HaystackClientApplication)getApplication();

        Intent intent = getIntent();
        m_pointDis = intent.getStringExtra("dis");

        setTitle(m_pointDis);

        String response = m_app.read("dis==\""+ m_pointDis + "\"");

        if (response.equals("")) {
            intent = new Intent(m_app.getApplicationContext(), HaystackClientActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);
            return;
        }

        HZincReader reader = new HZincReader(response);
        HGrid grid = reader.readGrid();
        HRow row = grid.row(0);
        HVal val = row.get("cur", false);
        if (val != null) {
            ((LinearLayout)findViewById(R.id.loutWatchPoll)).setVisibility(View.VISIBLE);
            m_tvCurrentValue = (TextView)findViewById(R.id.tvCurrentValue);
        }

        val = row.get("writable", false);
        if (val != null) {
            ((LinearLayout)findViewById(R.id.loutPointWrite)).setVisibility(View.VISIBLE);
        }

        m_handler = new Handler() {
            public void handleMessage(Message msg) {
                m_tvCurrentValue.setText(m_currentValue);
            }
        };
    }

    public void watchSub(View v) {
        String response = m_app.watchSub("Watch1", m_pointDis);
        Log.d("haystackClient", response);

        v.setEnabled(false);

        if (!response.equals("")) {
            HZincReader reader = new HZincReader(response);
            HGrid grid = reader.readGrid();
            HDict dict = grid.meta();
            HVal val = dict.get("watchId");

            m_watchId = val.toString();
            Log.d("haystackClient", m_watchId);
        }
        m_timer = new Timer();
        m_timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (m_app.getSessionLost()) {
                    Intent intent = new Intent(m_app.getApplicationContext(), HaystackClientActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    finish();
                    startActivity(intent);
                    return;
                }
                String response = m_app.watchPoll(m_watchId);
                Log.d("haystackClient", response);
                if (response.equals("")) {
                    return;
                }
                HZincReader reader = new HZincReader(response);
                HRow row = reader.readGrid().row(0);
                HVal val = row.get("curVal");
                Log.d("haystackClient", val.toString());
                m_currentValue = val.toString();
                m_handler.obtainMessage(1).sendToTarget();
            }
        }, 0, 5000);
    }

    public void pointWrite(View v) {
        EditText e = (EditText)findViewById(R.id.editPointValue);
        Log.d("haystackClient", "PointWrite: " + m_pointDis + " " + e.getText().toString());
        String response = m_app.pointWrite(m_pointDis, e.getText().toString());
        Log.d("haystackClient", response);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("haystackClient", "onStop()");
        if (m_timer != null) {
            m_timer.cancel();
            m_timer.purge();
        }
    }
}
