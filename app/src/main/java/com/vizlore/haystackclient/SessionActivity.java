/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.projecthaystack.*;
import org.projecthaystack.io.HZincReader;

import java.util.ArrayList;

public class SessionActivity extends AppCompatActivity {

    private HaystackClientApplication m_app;
    private SoftAPDetails m_serviceInfoData;
    private Button m_btnJoinSession;
    private ArrayAdapter<String> m_pointsAdapter;
    public Handler m_handler;
    public ProgressDialog m_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        m_btnJoinSession = (Button) findViewById(R.id.btnJoinSession);

        Intent intent = getIntent();
        int position = intent.getIntExtra("position", 0);

        m_app = (HaystackClientApplication)getApplication();
        m_serviceInfoData = m_app.getDeviceByPos(position);

        setTitle(m_serviceInfoData.appName);

        TextView tvAboutService = (TextView) findViewById(R.id.tvAboutService);
        tvAboutService.setText("Device Name: " + m_serviceInfoData.deviceFriendlyName + "\n" + m_serviceInfoData.getAnnounce());

        m_app.setSessionActivityRef(this);

        ListView lvPoints = (ListView)findViewById(R.id.lvPoints);
        m_pointsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, new ArrayList<String>());
        lvPoints.setAdapter(m_pointsAdapter);
        lvPoints.setOnItemClickListener(new PointsListOnClickListener());

        m_handler = new Handler() {
            public void handleMessage(Message msg) {
                m_dialog.dismiss();
                if (msg.what == 0) {
                    Toast.makeText(m_app.getBaseContext(), "Join Session failed!", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }
                readAllPoints();
            }
        };
    }

    public void joinSession(View v) {
        m_app.joinSession(m_serviceInfoData);
        if (m_app.getSessionLost()) {
            Intent intent = new Intent(m_app.getApplicationContext(), HaystackClientActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);
        }
        m_btnJoinSession.setVisibility(View.INVISIBLE);
        m_dialog = getProgressDialog("Joining session");
    }

    public void readAllPoints() {
        ((TextView) findViewById(R.id.tvAboutService)).setText("No sensors found!");
        m_dialog = getProgressDialog("Getting sensor list");
        new Thread(new Runnable() {
            @Override
            public void run() {
                final String result = m_app.read("point");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        m_dialog.dismiss();
                        displayPoints(result);
                    }
                });

            }
        }).start();
    }

    public void displayPoints(String zincData) {
        Log.d("haystackClient", "Read all points!");
        m_dialog.dismiss();

        HZincReader reader = new HZincReader(zincData);
        try {
            HGrid grid = reader.readGrid();

            for (int i = 0; i < grid.numRows(); i++) {
                String dis = grid.row(i).get("dis", true).toString();
                m_pointsAdapter.add(dis);
            }

            if (grid.numRows() > 0) {
                ((TextView) findViewById(R.id.tvAboutService)).setVisibility(View.INVISIBLE);
            }

            m_pointsAdapter.notifyDataSetChanged();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ProgressDialog getProgressDialog(String title) {
        ProgressDialog pd = new ProgressDialog(this);
        pd.setTitle(title);
        pd.setMessage("Please wait");
        pd.show();
        return pd;
    }

    @Override
    protected void onStop() {
        super.onStop();
        //m_app.leaveSession();
        //m_btnJoinSession.setVisibility(View.VISIBLE);
    }

    public boolean isSessionLost() {
        return m_app.getSessionLost();
    }
}
