/**
 * Copyright (C) 2016 VizLore LLC
 * This file is part of WiOcean Bridge application.
 *
 *  WiOcean Bridge application is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  WiOcean Bridge application is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with WiOcean Bridge application.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.vizlore.haystackclient;

import org.alljoyn.bus.BusException;
import org.alljoyn.bus.annotation.BusInterface;
import org.alljoyn.bus.annotation.BusMethod;

/**
 * Created by Pedja on 2/25/2016.
 */
@BusInterface(name = "com.vizlore.haystack.service.interface")
public interface HaystackService {
    @BusMethod
    public String read(String filter) throws BusException;
    @BusMethod
    public String watchPoll(String watchId) throws BusException;
    @BusMethod
    public String watchSub(String watchDis, String id) throws BusException;
    @BusMethod
    public String pointWrite(String dis, int level, String val, String who, String duration) throws BusException;
}
